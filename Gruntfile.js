/*global require, chalk, module*/

var _ = require( "lodash" );

//--
//-- watch
//-- jshint prod / dev
//-- csslint strict / lax
//-- uglify js
//-- scss
//-- cssmin

module.exports = function( grunt ){
	
	grunt.initConfig({
		"pkg": grunt.file.readJSON("package.json"),
		
		//-- check if there are errors in the JS file
		//-- jshint default options, NOTE: files are updated with the runner
		"jshint": {
			lax: {
				options: {
					globals: {
						jQuery: true,
						console: false,
						module: true,
						document: true
					}
				},
				src: "<%= pkg.jshint.src %>"
			}
		},
		
		//-- verify that css is correct
		"csslint": {
			lax: {
				options: {
				},
				src: "<%= pkg.csslint.src %>"
			}
		},
		
		/*
		//-- concat options,  NOTE: files are updated with the runner
		//-- concat no longer needed as mangle performs concat as-well.
		"concat": {
			devMode: {
				options: {
					separator: ';'
				},
				files: "<%= pkg.concat.js %>"
			},
			prodMode: {
				options: {
					separator: ';'
				},
				files: "<%= pkg.concat.js %>"
			},
			concatSCSS: {
				options: {
					separator: ';'
				},
				files: "<% pkg.concat.scss %>"
			}
		},
		*/
		
		//-- use mangle: to obfuscate the code, ex:
		//-- options { mangle: { except: ['jQuery','Backbone',...] } }
		"uglify": {
			devMode: {
				options: {
					mangle: false,
					sourceMap: true,
					sourceMap: true,
					preserveComments: "some"
				},
				files: "<%= pkg.concat.js %>"
			},
			prodMode: {
				options: {
					mangle: false,
					sourceMap: true,
					sourceMap: true,
					preserveComments: "some"
				},
				files: "<%= pkg.concat.js %>"
			}
		},
		
		
		sass: {
			devMode: {
				options: {
					style: 'expanded'
				},
				files: "<%= pkg.sass.files %>"
			},
			prodMode: {
				options: {
					style: 'expanded'
				},
				files: "<%= pkg.sass.files %>"
			}
		},
		
		
		watch: {
			jsSrc: {
				files: "<%= pkg.watch.js %>",
				options: {
					msg: "js file just changed"
				}
			},
			cssSrc: {
				files: "<%= pkg.watch.scss %>",
				options: {
					msg: "scss files changed"
				}
			}
		}
	});
	
	grunt.loadNpmTasks( 'grunt-contrib-copy' );
	grunt.loadNpmTasks( 'grunt-contrib-watch' );
	grunt.loadNpmTasks( 'grunt-contrib-jshint' );
	grunt.loadNpmTasks( 'grunt-contrib-concat' );
	grunt.loadNpmTasks( 'grunt-contrib-uglify' );
	grunt.loadNpmTasks( 'grunt-contrib-sass' );
	grunt.loadNpmTasks( 'grunt-contrib-csslint' );
	
	var pkgJSON = require( "./package.json" );
	
	//-- combines jsBuild/scssBuild into the appropriate environment builds
	grunt.registerTask( 'buildDev', 'Runs the build in dev mode', _.union( pkgJSON.devJSBuild, pkgJSON.devSCSSBuild ) );
	grunt.registerTask( 'buildProd', 'Runs the build in production mode', _.union( pkgJSON.prodJSBuild, pkgJSON.prodSCSSBuild ) );
	
	//-- defines which environment is captured, and therefore which tasks are run during watch.
	if( pkgJSON.environment === "PROD" ){
		grunt.log.writeln( "pkgJSON.environment:" + pkgJSON.environment );
		grunt.config.set( "watch.jsSrc.tasks", pkgJSON.prodJSBuild );
		grunt.config.set( "watch.cssSrc.tasks", pkgJSON.prodSCSSBuild );
	} else if( pkgJSON.environment === "DEV" ){
		grunt.log.writeln( "pkgJSON.environment:" + pkgJSON.environment );
		grunt.config.set( "watch.jsSrc.tasks", pkgJSON.devJSBuild );
		grunt.config.set( "watch.cssSrc.tasks", pkgJSON.devSCSSBuild );
	} else {
		grunt.log.writeln( "Unknown grunt environment:" + pkgJSON.environment + " - assumed DEV" );
	}
	
	grunt.registerTask( 'logMsg', 'A simple task to log stuff.', function(){
		if( arguments.length === 0 ){
			grunt.log.writeln( this.name + ":no args sent - " + JSON.stringify( this ) );
		} else if( arguments.length === 1 ){
			grunt.log.writeln( this.name + ":" + arguments[0] );
		} else {
			grunt.log.writeln( this.name + ":" + JSON.stringify( arguments ) );
		}
	});
};